# OpenMultiMaps [![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0) [![Crowdin](https://badges.crowdin.net/openmaps/localized.svg)](https://crowdin.com/project/openmaps) [![pipeline status](https://framagit.org/tom79/openmaps/badges/develop/pipeline.svg)](https://framagit.org/tom79/openmaps/commits/develop)

A simple client to display maps from [OpenStreetMap](https://www.openstreetmap.org).

[<img alt='Get it on F-Droid' src='./img/get-it-on-fdroid.png' height="80"/>](https://f-droid.org/packages/app.fedilab.openmaps/)

<img src="./img/img1.png" width="250">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<img src="./img/img2.png" width="250">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<img src="./img/img3.png" width="250">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<img src="./img/img4.png" width="250">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

## Issues
Issue tracker: [framagit.org/tom79/openmaps/issues](https://framagit.org/tom79/openmaps/issues)

## Releases
[Releases](https://framagit.org/tom79/openmaps/-/tags)

## Translations
Translations are done with [Crowdin](https://crowdin.com/project/openmaps), if a language is missing, please open an issue.

## Used maps

### Transports
1. [Openrouteservice](https://maps.openrouteservice.org)
1. [CyclOSM](https://www.cyclosm.org)
1. [OpenTopoMap](https://opentopomap.org)
1. [Free Parking](http://www.freeparking.world)
1. [OpenFuelMap](https://openfuelmap.net)
1. [OpenRailwayMap](https://www.openrailwaymap.org/mobile.php)
1. [Ben Maps](https://benmaps.fr)

### Life-skills
1. [OpenVegeMap](https://openvegemap.netlib.re)
1. [wheelmap](https://wheelmap.org)
1. [OpenBeerMap](https://openbeermap.github.io)
1. [OpenSolarMap](https://opensolarmap.org/)
1. [OpenWeatherMap](https://openweathermap.org/weathermap?basemap=map&cities=true&layer=clouds)
1. [Qwant Maps](https://www.qwant.com/maps/)


### Hobbies
1. [OpenSnowMap](http://opensnowmap.org/)
1. [HistOSM](https://histosm.org/)

### Regional
1. [Breton map](https://kartenn.openstreetmap.bzh)
1. [Occitan map](https://tile.openstreetmap.fr/?zoom=9&lat=43.82067&lon=1.235&layers=0000000B0FFFFFF)
1. [Basque map](https://tile.openstreetmap.fr/?zoom=11&lat=43.36289&lon=-1.45081&layers=00000000BFFFFFF)


### Contributions
1. [OpenStreetMap](https://www.openstreetmap.org)
1. [MapContrib](https://www.mapcontrib.xyz)
1. [OpenAdvertMap](https://openadvertmap.pavie.info)
1. [OpenLevelUp](https://openlevelup.net)
1. [OSM Then And Now](https://mvexel.github.io/thenandnow/)
1. [OsmHydrant](https://www.osmhydrant.org)
1. [OpenWhateverMap](http://openwhatevermap.xyz/)
1. [Open Infrastructure Map](https://openinframap.org)

## Credits
Idea from [@Bristow_69@framapiaf.org](https://framapiaf.org/@Bristow_69)
